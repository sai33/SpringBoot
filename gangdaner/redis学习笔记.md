[TOC]



## redis学习笔记

### 1.使用java代码操作redis-----JedisCluster

#### string

```java
	jedis.set(key,value);
	jedis.setex(key,seconds);	
```

#### list

#### set

#### hash

#### zSet

```java
jedis.zrange(key,start_index,end_index);//return Set<String>
jedis.zscore(key,value);//return Double score
/**
min,是指要检索的排序后的集合中的最小值，例如存入zadd(key,0,"v1");想从v1开始查询，则赋值"(v1"(开区间) 		或者"[v1"（闭区间），最小值用"-"代替
max,同min,最大值可以用 "+"
num，为想要获取的值的个数
*/
jedis.zrangByLex(key,min,max,offset,num);//return Set<String>
jedis.zrangWithScores(key,start,end);//return Set<Tuple>
/**
Tuple为元组,存入的 value,score
如果Tuple直接打印，则打印出的是[[value的byteArray,score],[[],score]]
*/
```

